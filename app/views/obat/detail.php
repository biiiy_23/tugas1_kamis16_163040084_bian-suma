<div class="container mt-5">
	<div class="card" style="width: 18rem;">
		<div class="card-body">
			<h5 class="card-title"><?= $data['obat']['nama_obat'] ?></h5>
			<h6 class="card-subtitle mb-2 text-muted">(<?= $data['obat']['kode_obat'] ?>)</h6>
			<p class="card-text"><?= $data['obat']['jenis_obat'] ?></p>
			<p class="card-text">Quantity : <?= $data['obat']['quantity'] ?></p>
			<p class="card-text">Rp. <?= $data['obat']['harga'] ?></p>
			<a href="<?= BASEURL ?>/obat" class="card-link">Kembali</a>
		</div>
	</div>
</div>