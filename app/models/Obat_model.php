<?php 

/**
 * 
 */
class Obat_model
{

	private $table = 'obat';
	private $db;
	private $kode_obat;
	private $nama_obat;
	private $jenis_obat;
	private $quantity;
	private $harga;

	public function __construct()
	{
		$this->db = new Database;
	}

	public function getAllObat()
	{
		$this->db->query("SELECT * FROM " . $this->table . " ORDER BY id");
		return $this->db->resultSet();
	}

	public function getObatById($id)
	{
		$this->db->query("SELECT * FROM " . $this->table . " WHERE id=:id");
		$this->db->bind("id", $id);
		return $this->db->single();
	}

	public function deleteObat($id) {
		$this->db->query("DELETE FROM " . $this->table . " WHERE id=:id");
		$this->db->bind('id', $id);
		$this->db->execute();
	}

	public function addObat($info) {
		$kode_obat = $info['kode_obat'];
		$nama_obat = $info['nama_obat'];
		$jenis_obat = $info['jenis_obat'];
		$quantity = $info['quantity'];
		$harga = $info['harga'];

		$this->db->query("INSERT INTO "	 . $this->table . " (id, kode_obat, nama_obat, jenis_obat, quantity, harga) VALUES ('', :kode_obat, :nama_obat, :jenis_obat, :quantity, :harga)");
		$this->db->bind('kode_obat', $kode_obat);
		$this->db->bind('nama_obat', $nama_obat);
		$this->db->bind('jenis_obat', $jenis_obat);
		$this->db->bind('quantity', $quantity);
		$this->db->bind('harga', $harga);
		$this->db->execute();
	}

	public function searchObat($keyword) {
		$this->db->query("SELECT * FROM " . $this->table . " WHERE kode_obat LIKE :keyword OR nama_obat LIKE :keyword OR jenis_obat LIKE :keyword OR quantity LIKE :keyword");
		$this->db->bind('keyword', $keyword);
		return $this->db->resultSet();
	}

	public function changeObat($info) {
		$id = $info['id'];
		$kode_obat = $info['kode_obat'];
		$nama_obat = $info['nama_obat'];
		$jenis_obat = $info['jenis_obat'];
		$quantity = $info['quantity'];
		$harga = $info['harga'];

		$this->db->query("UPDATE "	 . $this->table . " SET nama_obat = :nama_obat, kode_obat = :kode_obat, jenis_obat = :jenis_obat, quantity = :quantity, harga = :harga WHERE 
			id = :id");
		$this->db->bind('id', $id);
		$this->db->bind('kode_obat', $kode_obat);
		$this->db->bind('nama_obat', $nama_obat);
		$this->db->bind('jenis_obat', $jenis_obat);
		$this->db->bind('quantity', $quantity);
		$this->db->bind('harga', $harga);
		$this->db->execute();
	}
}