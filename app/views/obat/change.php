<div class="container mt-4">
	<div class="row">
		<div class="col-6">
			<h2><?= $data['judul'] ?></h3><br>
			<form enctype="multipart/form-data" method="POST" action="<?= BASEURL ?>/obat/change/<?= $data['obat']['id'] ?>">
				<table>
					<thead>
						<tr>
							<th></th>
							<th></th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td><input size="30px" type="hidden" id="id" name="id" value="<?= $data['obat']['id'] ?>"></td>
						</tr>
						<tr>
							<td height="60px"><label for="kode_obat">Kode Obat</label></td>
							<td width="20px">:</td>
							<td><input size="30px" type="text" id="kode_obat" name="kode_obat" value="<?= $data['obat']['kode_obat'] ?>"></td>
						</tr>
						<tr>
							<td height="60px"><label for="nama_obat">Nama Obat</label></td>
							<td width="20px">:</td>
							<td><input size="30px" type="text" id="nama_obat" name="nama_obat" value="<?= $data['obat']['nama_obat'] ?>"></td>
						</tr>
						<tr>
							<td height="60px"><label for="jenis_obat">Jenis Obat</label></td>
							<td width="20px">:</td>
							<td><input size="30px" type="text" id="jenis_obat" name="jenis_obat" value="<?= $data['obat']['jenis_obat'] ?>"></td>
						</tr>
						<tr>
							<td height="60px"><label for="quantity">Quantity</label></td>
							<td width="20px">:</td>
							<td><input size="30px" type="text" id="quantity" name="quantity" value="<?= $data['obat']['quantity'] ?>"></td>
						</tr>
						<tr>
							<td height="60px"><label for="harga">Harga</label></td>
							<td width="20px">:</td>
							<td><input size="30px" type="text" id="harga`" name="harga" value="<?= $data['obat']['harga'] ?>"></td>
						</tr>
					</tbody>
				</table><br>

				<button type="submit" name="submit" id="submit" class="btn btn-primary">Ubah</button>
				<a href="<?= BASEURL ?>/obat"><button type="button" class="btn">Cancel</button></a>
			</form>
		</div>
	</div>
</div>