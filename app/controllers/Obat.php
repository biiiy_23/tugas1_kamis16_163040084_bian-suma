<?php 

/**
 * 
 */
class Obat extends Controller
{
	public function index()
	{
		$keyword = "";

		if (isset($_POST['search'])) {
			$keyword = '%' . $_POST['search'] . '%';
			$data = array(
				'judul' => "Daftar Data Obat",
				'obat' => $this->model('Obat_model')->searchObat($keyword)
			);
			$this->view('templates/header', $data);
			$this->view('obat/index', $data);
			$this->view('templates/footer');
		} else {
			$data = array(
				'judul' => "Daftar Data Obat",
				'obat' => $this->model('Obat_model')->getAllObat()
			);
		$this->view('templates/header', $data);
		$this->view('obat/index', $data);
		$this->view('templates/footer');
		}
	}

	public function detail($id)
	{
		$data = array(
			'judul' => "Detail Data Obat",
			'obat' => $this->model('Obat_model')->getObatById($id)
		);
		$this->view('templates/header', $data);
		$this->view('obat/detail', $data);
		$this->view('templates/footer');
	}

	public function change($id) {

		$data = [
			'judul' => "Ubah Data Obat",
			'obat' => $this->model('Obat_model')->getObatById($id)
		];

		if (isset($_POST['submit'])) {
			$info['id'] = $_POST['id'];
			$info['kode_obat'] = $_POST['kode_obat'];
			$info['nama_obat'] = $_POST['nama_obat'];
			$info['jenis_obat'] = $_POST['jenis_obat'];
			$info['quantity'] = $_POST['quantity'];
			$info['harga'] = $_POST['harga'];

			$this->model('Obat_model')->changeObat($info);
			header("Location: ../index");
		}

		$this->view('templates/header', $data);
		$this->view('obat/change', $data);
		$this->view('templates/footer');
	}

	public function add() {

		$data = array(
			'judul' => "Tambah Data Obat"
		);

		if (isset($_POST['submit'])) {
			$info['kode_obat'] = $_POST['kode_obat'];
			$info['nama_obat'] = $_POST['nama_obat'];
			$info['jenis_obat'] = $_POST['jenis_obat'];
			$info['quantity'] = $_POST['quantity'];
			$info['harga'] = $_POST['harga'];
			$this->model('Obat_model')->addObat($info);
			header("Location: ../index");
		}

		$this->view('templates/header', $data);
		$this->view('obat/add', $data);
		$this->view('templates/footer');	
	}

	public function delete($id) {
		$this->model('Obat_model')->deleteObat($id);
		header("Location: ../index");
	}
}