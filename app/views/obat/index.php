<div class="container mt-5">
	<div class="row">
		<div class="col-6">
			<h3><?= $data['judul'] ?></h3>
			<table class="table table-hover">
			<thead class="thead-light">
				<tr>
					<th>#</th>
					<th colspan="3">Nama</th>
					<th colspan="3">Action</th>
				</tr>
			</thead>

			<tbody>
			<?php if (empty($data['obat'])) { ?>
			<tr>
				<td align="center" colspan="5">Maaf, Data tidak ditemukan</td>
			</tr>
			<?php } else { ?>
			<?php $i = 1; ?>
			<?php foreach ($data['obat'] as $key) : ?>
			<tr>
				<td><?= $i?></td>
				<td><?= $key['nama_obat']?></td>
				<td colspan="2"><a href="<?= BASEURL ?>/obat/detail/<?= $key['id'] ?>"><button type="button" class="btn">Detail</button></a></td>
				<td><a href="<?= BASEURL ?>/obat/change/<?= $key['id'] ?>"><button type="button" class="btn btn-success">Change</button></a></td>
				<td><a href="<?= BASEURL ?>/obat/delete/<?= $key['id'] ?>"><button type="submit" class="btn btn-danger" onclick="return confirm('Anda yakin ingin menghapus data ini?')">Delete</button></a></td>
			</tr>

			<?php $i++; endforeach; ?>
			<?php } ?>	
			</tbody>
			</table>
		</div>

		<div class="col-6">
			<a href="<?= BASEURL ?>/obat/add"><button type="button" class="btn btn-primary">Tambah Data</button></a>	
		</div>
	</div>
</div>

