-- MySQL dump 10.16  Distrib 10.1.19-MariaDB, for Win32 (AMD64)
--
-- Host: localhost    Database: localhost
-- ------------------------------------------------------
-- Server version	10.1.19-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `obat`
--

DROP TABLE IF EXISTS `obat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `obat` (
  `id` int(1) NOT NULL AUTO_INCREMENT,
  `kode_obat` varchar(8) NOT NULL,
  `nama_obat` varchar(32) NOT NULL,
  `jenis_obat` varchar(10) NOT NULL,
  `quantity` int(3) NOT NULL,
  `harga` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `obat`
--

LOCK TABLES `obat` WRITE;
/*!40000 ALTER TABLE `obat` DISABLE KEYS */;
INSERT INTO `obat` VALUES (1,'b-001','Bodrex','Tablet',5,10000),(2,'b-002','Diapet','Tablet',3,1500),(3,'b-003','Mixagrip','Tablet',1,1500),(4,'b-004','Promag','Tablet',7,13000),(5,'b-006','Amlodipin','Tablet',3,26000),(6,'b-007','Amoxilyn','Tablet',1,4500),(7,'b-008','Anakonidin','Cair',1,9900),(8,'b-009','Apialys Drop','Cair',2,31600),(9,'b-010','Asam Menefamat','Tablet',3,22000),(10,'b-011','Bisolvon','Cair',8,27000),(11,'b-012','Counterpain','Salep',1,21000),(12,'b-013','Avodart','Kapsul',1,311000),(13,'b-014','Azmacon','Tablet',3,66500),(14,'b-015','Azopt Tetes Mata','Cair',6,15000),(15,'b-016','Astifen ','Sirup',1,13000),(16,'b-017','Astin Force ','Kapsul',1,156000),(17,'b-018','Poldamix','Kapsul',25,5000),(18,'b-019','PCC','Pil',300,100000);
/*!40000 ALTER TABLE `obat` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-10-24  7:28:40
